import { check } from "express-validator";

const validateRegister = () => {
    return [
        check("name", "Name is required").not().isEmpty(),
        check("email", "Please include a valid email").isEmail(),
        check(
            "password",
            "Please enter a password with 6 or more characters"
        ).isLength({ min: 8 }),
        check("re_password", "Password not match").custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error("Password not match");
            }
            return true;
        }),
    ];
};

const validateLogin = () => {
    return [
        check("email", "Please include a valid email").isEmail(),
        check("password", "Password is required").not().isEmpty(),
    ];
};

const validateProfile = () => {
    return [
        check("name", "Name is required").not().isEmpty(),
        check("avatar", "Avatar is required").not().isEmpty(),
    ];
};

const validateChallenge = () => {
    return [
        check("title", "Name is required").not().isEmpty(),
        check("description", "Description is required").not().isEmpty(),
        check("suggestion", "Suggestion is required").not().isEmpty(),
        check("name_function", "Name function is required").not().isEmpty(),
        check("params", "Params is required").not().isEmpty().isArray(),
        check("params.*.name", "Name is required").not().isEmpty(),
        check("params.*.type", "Type is required").not().isEmpty(),
        check("params.*.index", "Index is required").not().isEmpty(),
        check("test_case", "Code temps is required").not().isEmpty().isArray(),
        check("test_case.*.input.*.name", "Name input in test case is required").not().isEmpty(),
        check("test_case.*.input.*.value", "Value input in test case is required").not().isEmpty(),
        check("test_case.*.expect", "Expect test case is required").not().isEmpty(),
        check("test_case.*.hidden", "Hidden test case is required").not().isEmpty(),
        check("run_limit_seconds", "Limit seconds is required").not().isEmpty(),
        check("run_limit_memory", "Limit memory is required").not().isEmpty(),
        check("output_type", "Output type is required").not().isEmpty(),
        check("rank", "Rank is required").not().isEmpty().isIn([1, 2, 3]),
        check("score", "Score is required").not().isEmpty().isInt(),
    ];
};

const validateRunCode = () => {
    return [
        check("code", "Code is required").not().isEmpty(),
        check("language", "Language is required").not().isEmpty().isIn(["c", "python3", "java", "javascript", "php"]),
    ];
};

export {
    validateRegister,
    validateLogin,
    validateProfile,
    validateChallenge,
    validateRunCode
};
