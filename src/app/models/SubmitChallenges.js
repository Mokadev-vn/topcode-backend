import mongoose from "mongoose";

const Schema = mongoose.Schema;

const submitChallengesSchema = new Schema({
    challenge_id: {
        type: Schema.Types.ObjectId, 
        ref: 'Challenges' 
    },
    user_id: {
        type: Schema.Types.ObjectId, 
        ref: 'User' 
    },
    code_text: {
        type: String,
        default: ""
    },
    language: {
        type: String,
        require: true
    },
    status: {
        type: Number,
        default: 0
    },
    score: {
        type: Number,
        default: 0
    },
    count_submit: {
        type: Number,
        default: 1
    },

},  { timestamps: {}, strict: true });

const SubmitChallenges = mongoose.model('SubmitChallenges', submitChallengesSchema)
export default SubmitChallenges