import mongoose from "mongoose";

const Schema = mongoose.Schema;

const caseSchema = new Schema({
    input: [
        {
            name: {
                type: String,
                required: true,
                trim: true
            },
            value: {
                type: String,
                required: true,
                trim: true
            }
        }
    ],
    expect:
    {
        type: String,
        required: true,
        trim: true
    },
    challenge_id: {
        type: Schema.Types.ObjectId,
        ref: 'Challenges'
    },
    hidden: {
        type: Boolean,
        default: false
    },
},  { timestamps: {}, strict: true });


const Cases = mongoose.model('Cases', caseSchema)
export default Cases