import mongoose from "mongoose";

const Schema = mongoose.Schema;

const notificationsSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    status: {
        type: Number,
        default: 0,
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    send_by: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
},  { timestamps: {}, strict: true });

const Notifications = mongoose.model('Notifications', notificationsSchema)
export default Notifications