import Challenges from "../models/Challenges.js";
import Config from "../models/Config.js";
import User from "../models/User.js";

const getChallengesNew = async (req, res) => {
    try {
        const challenges = await Challenges.find({ status: 1, type: 0 }, null, {
            limit: 10,
        })
            .select(
                "title description description_en suggestion suggestion_en rank score"
            )
            .populate("created_by", "_id name avatar");
        res.status(200).json({ status: "success", data: challenges });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const getChallenges = async (req, res) => {
    try {
        const resultsPerPage = 15;
        let page = Number(req.query.page) >= 1 ? Number(req.query.page) : 1;
        const query = {
            status: 1,
            type: 0,
        };

        if(req.query.rank) {
            query.rank = req.query.rank;
        }

        if(req.query.title) {
            query.title = { $regex: req.query.title, $options: 'i' };
        }

        const totalChallenges = await Challenges.find(query).countDocuments();

        const challenges = await Challenges.find(query)
            .select(
                "title description description_en suggestion suggestion_en rank score"
            )
            .populate("created_by", "_id name avatar")
            .sort({ createdAt: -1 })
            .limit(resultsPerPage)
            .skip(resultsPerPage * (page - 1));
        const totalPages = Math.ceil(totalChallenges / resultsPerPage);
        const pagination = {
            count: challenges.length,
            next_page: page + 1,
            current_page: page,
            total: totalChallenges,
            total_pages: totalPages,
        };

        res.status(200).json({
            status: "success",
            data: challenges,
            pagination,
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const dashboard = async (req, res) => {
    try {
        const challengesNew = await Challenges.find(
            { status: 1, type: 0 },
            null,
            {
                limit: 10,
            }
        )
            .select(
                "title description description_en suggestion suggestion_en rank score"
            )
            .populate("created_by", "_id name avatar");
        const config = await Config.findOne();

        res.status(200).json({
            status: "success",
            data: { challengesNew, config },
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const rank = async (req, res) => {
    try {
        const rank = await User.find({}, null, {
            limit: 10,
            sort: { score: -1 },
        }).select("_id name rank score");
        
        res.status(200).json({ status: "success", data: rank });
        
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

export { getChallengesNew, getChallenges, dashboard, rank };
