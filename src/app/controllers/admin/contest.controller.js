import Contests from "../../models/Contests.js";
import { languages } from "../../../config/app.js";
import { renderCodeFunction } from "../../../utils/helper.js";
import Challenges from "../../models/Challenges.js";
import Cases from "../../models/Cases.js";

const listContest = async (req, res) => {
    try {
        const resultsPerPage = 15;
        let page = Number(req.query.page) >= 1 ? Number(req.query.page) : 1;
        const totalContest = await Contests.find().countDocuments();
        const contests = await Contests.find()
            .select(
                "title avatar time_start time_end description status time_register time_register"
            )
            .populate("user_join", "_id name avatar")
            .populate("created_by", "_id name avatar")
            .sort({ name: "asc" })
            .limit(resultsPerPage)
            .skip(resultsPerPage * (page - 1));
        const totalPages = Math.ceil(totalContest / resultsPerPage);
        const pagination = {
            count: contests.length,
            next_page: page + 1,
            current_page: page,
            total: totalContest,
            total_pages: totalPages,
        };

        res.status(200).json({
            status: "success",
            data: contests,
            pagination,
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const createContest = async (req, res) => {
    try {
        const {
            title,
            avatar,
            description,
            time_start,
            time_register,
            time_register_end,
            password,
            time_end,
            challenges,
        } = req.body;

        const challengesId = [];
        for (let i = 0; i < challenges.length; i++) {
            const {
                title,
                description,
                description_en,
                suggestion,
                suggestion_en,
                score,
                run_limit_seconds,
                run_limit_memory,
                test_case,
                name_function,
                params,
            } = challenges[i];

            const code_temps = [];

            for (let i = 0; i < languages.length; i++) {
                const { code } = languages[i];
                const codeRender = renderCodeFunction(
                    name_function,
                    params,
                    code
                );
                code_temps.push({
                    lang: code,
                    code: codeRender,
                });
            }

            const challenge = await Challenges.create({
                title,
                description,
                description_en,
                suggestion,
                suggestion_en,
                name_function,
                params,
                code_temps,
                score,
                type: 1,
                status: 1,
                run_limit_seconds,
                run_limit_memory,
                created_by: req.user._id,
            });

            for (let i = 0; i < test_case.length; i++) {
                await Cases.create({
                    challenge_id: challenge._id,
                    input: test_case[i].input,
                    expect: test_case[i].expect,
                    hidden: test_case[i].hidden,
                });
            }

            challengesId.push(challenge._id);
        }

        const contest = await Contests.create({
            title,
            avatar,
            description,
            time_start,
            time_register,
            time_register_end,
            time_end,
            password,
            status: 0,
            user_join: [],
            challenge_join: challengesId,
            created_by: req.user._id,
        });

        res.status(200).json({ status: "success", data: contest });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const updateContest = async (req, res) => {
    try {
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const detailContest = async (req, res) => {
    try {
        const contest = await Contests.find({ _id: req.params.id });
        if (!contest) {
            throw new Error("Contest not found");
        }
        res.status(200).json({
            status: "success",
            data: contest,
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

export { listContest, createContest, detailContest, updateContest };
