import express from "express";
import { getConfig } from "../app/controllers/admin/config.controller.js";
import {
    getChallengesNew,
    getChallenges,
    dashboard,
    rank,
} from "../app/controllers/home.controller.js";
import { auth } from "../app/middleware/auth.js";

const router = express.Router();

router.get("/challenge-new", getChallengesNew);
router.get("/challenges", getChallenges);
router.get("/dashboard", auth, dashboard);
router.get("/rank", auth, rank);
router.get("/config", getConfig);

export default router;
