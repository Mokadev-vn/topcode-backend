import express from "express";
import {
    login,
    register,
    profile,
    updateProfile,
    logout,
} from "../app/controllers/auth.controller.js";
import {
    createChallenge,
    updateChallenge,
    listChallengeUserCreate,
    detailChallenge,
    testCaseInChallenge,
    submitCaseInChallenge,
    challengeJoined,
} from "../app/controllers/user/challenge.controller.js";
import { listContest, registerContest } from "../app/controllers/user/contest.controller.js";
import { auth } from "../app/middleware/auth.js";
import {
    validateChallenge,
    validateLogin,
    validateProfile,
    validateRegister,
    validateRunCode,
} from "../utils/validator.js";

const router = express.Router();

router.post("/login", validateLogin(), login);
router.get("/logout", auth, logout);
router.post("/register", validateRegister(), register);
router.put("/update-profile", auth, validateProfile(), updateProfile);
router.get("/profile", auth, profile);

router.post("/create-challenge", auth, validateChallenge(), createChallenge);
router.put("/update-challenge/:id", auth, validateChallenge(), updateChallenge);
router.get("/challenges", auth, listChallengeUserCreate);
router.get("/detail-challenge/:id", auth, detailChallenge);

router.post(
    "/solutions/:challenge_id",
    auth,
    validateRunCode(),
    testCaseInChallenge
);
router.post(
    "/submit/:challenge_id",
    auth,
    validateRunCode(),
    submitCaseInChallenge
);

router.get("/challenge-join", auth, challengeJoined);

router.get("/contests", auth, listContest);
router.post("/register-contest", auth, registerContest);

export default router;
